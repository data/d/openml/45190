# OpenML dataset: hepar2_4

https://www.openml.org/d/45190

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Hepar2 Bayesian Network. Sample 4.**

bnlearn Bayesian Network Repository reference: [URL](https://www.bnlearn.com/bnrepository/discrete-large.html#hepar2)

- Number of nodes: 70

- Number of arcs: 123

- Number of parameters: 1453

- Average Markov blanket size: 4.51

- Average degree: 3.51

- Maximum in-degree: 6

**Authors**: A. Onisko.

**Please cite**: ([URL](https://sites.pitt.edu/~druzdzel/psfiles/malbork.pdf)): A. Onisko. Probabilistic Causal Models in Medicine: Application to Diagnosis of Liver Disorders. Ph.D. Dissertation, Institute of Biocybernetics and Biomedical Engineering, Polish Academy of Science, Warsaw, March 2003.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45190) of an [OpenML dataset](https://www.openml.org/d/45190). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45190/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45190/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45190/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

